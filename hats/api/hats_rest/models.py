from django.db import models

class LocationVO(models.Model):
    import_href= models.CharField(max_length=200, default='')


class Hat (models.Model):
    fabric= models.CharField(max_length= 50)
    style_name= models.CharField(max_length=50)
    color= models.CharField(max_length=50)
    picture_url= models.URLField(max_length=200, null=True)
    location= models.ForeignKey(
        LocationVO,
        related_name= 'location',
        on_delete= models.PROTECT
    )

