from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import LocationVO, Hat



class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            import_href = content["location"]
            location = LocationVO.objects.get(import_href=import_href)
            print("HERE", location, import_href)
            content['location']= location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location href"},
                status  = 400
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder= HatDetailEncoder,
            safe= False,
        )


@require_http_methods(["DELETE", "GET"])
def api_hat(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            {"hat": hat},
            encoder=HatDetailEncoder,
        )
    else: 
        try: 
            hat= Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
        except Hat.DoesNotExist:
            response = JsonResponse(
                {"message": "No longer exist"}
            )
            return response