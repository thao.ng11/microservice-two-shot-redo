from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import BinVO, Shoe



class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
        "id",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            import_href = content["bin"]
            bin = BinVO.objects.get(import_href=import_href)
            print("HERE", bin, import_href)
            content['bin']= bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin href"},
                status  = 400
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder= ShoeDetailEncoder,
            safe= False,
        )


@require_http_methods(["DELETE", "GET"])
def api_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoeDetailEncoder,
        )
    else: 
        try: 
            shoe= Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
        except Shoe.DoesNotExist:
            response = JsonResponse(
                {"message": "No longer exist"}
            )
            return response